* Aktuální verze využívá soubory z http://kvap.tul.cz/rocenky/
 - - **Protože soubory jsou ze serveru, nefunguje funkce ukládání změn!**
* ## **Aplikace zde: https:** https://ocr-database.herokuapp.com/
# **Přehled aplikace**
* ## **Nynější vzhled**
![Nynější vzhled](/previews/preview04_a.png) 
![Nynější vzhled](/previews/preview04_b.png)

* ## **Ukázka č. 3**
![preview 03](/previews/preview03.png)

* ## **Ukázka č. 2**
![preview 02](/previews/preview02.png)

* ## **Ukázka č. 1**
![preview 01](/previews/preview01.png)

* ## **Ukázka č. 0**
![preview 00](/previews/preview.png)