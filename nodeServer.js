const http = require('http');
const app = require('./app');
const port = process.env.PORT || 8080;
const server = http.createServer(app);
server.listen(port, () => console.log("Server running at port " + port));
/*
const express = require('express');
const app = express();
const fs = require('fs');
let port = process.env.PORT || 8080;
app.listen(port, () => console.log('server running at port 8080'));
app.use(express.static('WebContent'));
app.use(express.json({ limit: '1mb' }));

app.post('/api', (request, response) => {
    let path = request.body.path;
    let content = request.body.content;
    fs.writeFile(path, content, (err) => {
        if (err) throw err;
        console.log('File saved as ' + path);
    });
    response.json({
        status: 'File saved',
        body: {
            Path: request.body.path,
            content: request.body.text
        }
    });
});

app.post('/api2', (request, response) => {
    console.log("incomming request");
    let files = request.body.array;
    let content = []
    for (let i = 0; i < files.length; i++) {
        content[i] = fs.readFileSync(files[i], "utf8");
    }
    console.log("files like " + files[0] + " etc...  Successfuly loaded");
    response.json({
        status: 'successful',
        body: { array: content }
    });
});*/