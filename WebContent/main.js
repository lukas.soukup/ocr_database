async function createList() {
    let lastPage, inhalt, content;
    let folders = await years_get_all();
    let years = getYears(folders); //obsahuje pole vsech roku
    let pages;
    dropdownTxtBox(years, "years");
    document.getElementById("year").onchange = async function() {
        let p = await years_get_one('r' + document.getElementById("year").value);
        pages = p;
        clearPageDropdown();
        dropdownTxtBox(pages.pages, "pages");
        lastPage = getLastPage(pages.pages);
        document.getElementById("page").value = 0;
        //srcRequest = getSrcIndex(years, pages);
        loadPageContent();
        document.getElementById("searchText").onchange();
        removeOldInhalt();
        inhalt = await pages_get_inhalt(this.value);
        formatInhaltFile(inhalt);
        removeMagnifier();
        let c = await pages_get_all_year(document.getElementById("year").value);
        content = c;
    };

    document.getElementById("page").onchange = function() {
        loadPageContent();
    };
    // to get to the first page <<
    document.getElementById("toStart").onclick = function() {
        let page = document.getElementById("page");
        page.value = 0;
        page.onchange();
        removeMagnifier();
    };
    //to get pravious page <
    document.getElementById("previous").onclick = function() {
        let page = document.getElementById("page");
        let index;
        if (page.value != 0) {
            for (i in pages.pages) {
                if (page.value == i) {
                    page.value = index;
                    page.onchange();
                    break;
                }
                index = i;
            }
        }
        //let pagesOfYear = getPage(years, pages);
        //let index = getIndexOfPage(pagesOfYear, page.value);
        removeMagnifier();
    };
    //to get next page >
    document.getElementById("next").onclick = function() {
        let page = document.getElementById("page");
        let found = false;
        for (i in pages.pages) {
            if (found) {
                page.value = i;
                page.onchange();
                break;
            }
            if (page.value == i) {
                found = true;
            }
        }
        removeMagnifier();
    };
    //to get to the last page >>
    document.getElementById("toEnd").onclick = function() {
        let page = document.getElementById("page");
        page.value = lastPage;
        page.onchange();
        removeMagnifier();
    };

    document.getElementById("searchText").onchange = function() {
        removeOldSearch();
        if (this.value == "") return;
        loadToSearchAll(content, years, pages);
    }
    document.getElementById("txt").addEventListener("input", function() {
        if (document.getElementById("autoSave").checked) {
            removeHighlights();
            //saveTextAsFile(pageList, srcRequest);
        }
    }, false);
    /*
        document.getElementById('save').onclick = () => {
            removeHighlights();
            saveTextAsFile(pageList, srcRequest);
            setTimeout(() => {
                document.getElementById('messageBox').style.opacity = '0';
            }, 2500);
        }*/
}

/**
 * soucasti UI.js v UI nefunguje
 */
function removeMagnifier() {
    let zoom = document.getElementById("magnify");
    if (!zoom.checked) return;
    document.getElementById("magnify").checked = false;
    var child = document.getElementById("img-magnifier-glass");
    var parent = child.parentElement;
    parent.removeChild(child);
}
/**
 * Metoda se stara o nacitani obrazku a textu na stranku 
 * @param {*} pageList xml soubory
 * @param {*} srcRequest index 
 */
async function loadPageContent() {
    let year = document.getElementById('year').value;
    let page = buildPageSrc(document.getElementById('page').value);
    let c = await pages_get_one('r' + year, 'r' + year + '_' + page);
    loadTxtFile(c);
}

function buildPageSrc(i) {
    i = i.split('_');
    if (!i[1]) i[1] = '';
    else i[1] = '_' + i[1];
    if (i[0].charAt(0) == 'Z') {
        return i[0] + i[1] + '.txt';
    }
    let num = i[0].match(/\d+/g);
    let letr = i[0].match(/[a-zA-Z]+/g);
    if (!letr) letr = '';
    if (num[0].length == 1) {
        return 's00' + num + letr + i[1] + '.txt';
    } else if (num[0].length == 2) {
        return 's0' + num + letr + i[1] + '.txt';
    } else {
        return 's' + num + letr + i[1] + '.txt';
    }
}
/**
 * Pomoci Ajax pristupu pres XMLHttpRequest otevira pozadovany textovy soubor a rovnou ho vklada do texboxu
 * @param {path to the page text file} fileName 
 */
function loadTxtFile(xhttp) {
    let content;
    for (i in xhttp) {
        if (i != 'year')
            content = xhttp[i];
    }
    document.getElementById("txt").innerHTML = content.txt;
    document.getElementById('jpg').src = 'http://kvap.tul.cz/rocenky/' + xhttp.year + '/' + content.jpgSrc;
    if (document.getElementById("searchText").value != "") {
        handleInput();
    }
}

function loadToSearchAll(content) {
    let keyWord = document.getElementById("searchText").value;
    let container = document.getElementById("searchAll");
    var re = new RegExp(keyWord, 'gi');
    let nOfMatchesPerPage = [];
    let btn, strAll, nOfAllMatches = 0;
    for (let i = 0; i < content.length; i++) {
        for (parm in content[i]) {
            if (parm == 'year') {} else {
                if (content[i][parm].txt.match(re) == null) nOfMatchesPerPage[i] = { occurance: 0, page: parm };
                else {
                    nOfMatchesPerPage[i] = { occurance: content[i][parm].txt.match(re).length, page: parm }
                    nOfAllMatches += content[i][parm].txt.match(re).length;
                }
            }
        }
    }
    let inserSort = insertSort(nOfMatchesPerPage);
    let nadpis = document.createElement("b");
    nadpis.innerHTML = "\"" + keyWord + "\"</br>";
    let totalNumber = document.createElement("small");
    let popis = document.createElement("i");
    popis.innerHTML = "Stránka : Četnost";
    popis.style.fontSize = "10px";
    container.appendChild(nadpis);
    container.appendChild(totalNumber);
    container.appendChild(popis);
    for (let i = 0; i < content.length; i++) {
        if (inserSort[i].occurance != 0) {
            btn = document.createElement("button");
            btn.id = inserSort[i].page;
            btn.className = "searching";
            btn.style.width = "100%";
            btn.style.textAlign = "left";
            btn.addEventListener("click", linkOnclickEventListener);
            strAll = document.createTextNode("#" + inserSort[i].page + ": " + inserSort[i].occurance);
            btn.appendChild(strAll);
            container.appendChild(btn);
        }
    }
    let buttons = document.getElementsByClassName("searching");
    totalNumber.innerHTML = buttons.length + " stránek z " + inserSort.length + "</br>Celkem " + nOfAllMatches + " výskytů</br>";
    if (buttons.length <= 0) {
        let notFoud = document.createElement("h1");
        notFoud.innerHTML = "žádný výsledek";
        notFoud.style.fontSize = "20px";
        container.appendChild(notFoud);
    }
}

function insertSort(array) {
    let j, tmp;
    for (let i = 0; i < array.length - 1; i++) {
        j = i + 1;
        tmp = array[j];
        while (j > 0 && tmp.occurance > array[j - 1].occurance) {
            array[j] = array[j - 1];
            j--;
        }
        array[j] = tmp;
    }
    return array;
}

function removeOldSearch() {
    let parent = document.getElementById("searchAll");
    while (parent.lastChild) {
        parent.removeChild(parent.lastChild);
    }
}

/**
 * Metoda vraci cestu k obrazku a textu na zadanem indexu
 * @param {*} pageList xml soubory
 * @param {*} srcRequest index 
 */
function findSrcRequest(pageList, srcRequest) {
    let jpgSrc, txtSrc;
    txtSrc = pageList[srcRequest].getElementsByTagName("txt")[0].childNodes[0].nodeValue;
    jpgSrc = pageList[srcRequest].getElementsByTagName("jpg")[0].childNodes[0].nodeValue;
    return { txt: txtSrc, jpg: jpgSrc }
}

/**
 * zformatuje text ze souboru v pc aby se mohl nacist i kdyz bude zobrazena jina stranka nez inhalt
 * pote kady inhalt rozdeli dle # a vztvori z nej interaktivni button
 * @param content -> obsah
 */
function formatInhaltFile(inhalt) {
    let content;
    for (i in inhalt) {
        content = inhalt[i].txt;
    }
    let str = [];
    let strAll;
    let id;
    let btn;
    str = content.split("\n");
    for (let i = 0; i < str.length; i++) {
        str[i] = str[i].replace(/[\r\n]+/gm, ""); //vymaze vsechny volna mista
        if (str[i] != "") {
            id = slizeStr(str[i]);
            btn = document.createElement("button");
            btn.id = id;
            btn.addEventListener("click", linkOnclickEventListener);
            btn.style.width = "100%";
            btn.style.textAlign = "left";
            strAll = document.createTextNode(str[i]);
            btn.appendChild(strAll);
            document.getElementById("ShowContent").appendChild(btn);
        }
    }
}

/**
 * Tato metoda je tu aby smazala drive vlozeny obsah a uvolnila tak misto pro novy
 */
function removeOldInhalt() {
    let parent = document.getElementById("ShowContent");
    while (parent.lastChild) {
        parent.removeChild(parent.lastChild);
    }
}

/**
 * metoda pro formatInhaltFile()
 * @param {*} str 
 */
function slizeStr(str) {
    let s = []
    s = str.split(" ");
    str = s[0];
    str = str.replace("#", "");
    return str;
}

/**
 * EventListener pro interaktivni buttony v obsahu
 */
function linkOnclickEventListener() {
    let page = this.id;
    document.getElementById("page").value = page;
    document.getElementById("page").onchange();
}

/**
 * metoda komunikuje se serverem a uklada do souboru v pc zmeny provedene v txt souborech
 * @param {*} pageList -> list vsech stran dle roku
 * @param {*} srcRequest -> pozadovana stranka
 *
async function saveTextAsFile(pageList, srcRequest) {
    let filePath = findSrcRequest(pageList, srcRequest).txt;
    filePath = 'WebContent\\\\' + filePath;
    let text = document.getElementById("txt").innerHTML;
    const data = { path: filePath, content: text };
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    };
    const response = await fetch('/api', options);
    const json = await response.json();
    if (json.status === 'File saved' && !document.getElementById('autoSave').checked) {
        document.getElementById('messageBox').style.opacity = '1';
    }
    console.log(json);
}*/

/**
 * Metoda vraci pouze pole vsech rocniku
 * @param {*} folders 
 */
function getYears(folders) {
    let years = [];
    for (i in folders.years) {
        years[i] = folders.years[i].replace('r', '');
        years[i] = years[i].replace('/', '');
    }
    return years;
}

/**
 * Vraci posledni index stranky z prave vzbraneho roku 
 * @param {*} pages 
 */
function getLastPage(pages) {
    let lastPage;
    for (i in pages) {
        lastPage = i;
    }
    return lastPage;
}
/**
 * Conected to the <datalist id="type"> in html file
 * Metoda vklada hodnoty primo do html 
 * @param value -> array 
 * @param type -> either "years" or "pages"
 */
function dropdownTxtBox(value, type) {
    var optionNode;
    if (type == 'years') {
        for (let i = 0; i < value.length; i++) {
            optionNode = document.createElement("option");
            optionNode.value = value[i];
            document.getElementById(type).appendChild(optionNode);
        }
        return;
    }
    for (i in value) {
        optionNode = document.createElement("option");
        optionNode.value = i;
        document.getElementById(type).appendChild(optionNode);
    }
}
/**
 * Metoda zamezuje opakovanemu pricitani dat do dropDown menu pro stranky
 */
function clearPageDropdown() {
    let pages = document.getElementById("pages").children;
    if (pages.length > 0) {
        document.getElementById("pages").removeChild(pages[0]);
        clearPageDropdown();
    }
}
createList();