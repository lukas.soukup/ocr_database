/**
 * vraci json seznam s rocniky
 * {"count": 50,
    "years": [
        "r1892/",
           ...
        "r1941/"]
    }
 */
function years_get_all() {
    const options = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    };
    return fetch('get/', options)
        .then(res => res.json())
        .then(res => {
            return res;
        }).catch(err => {
            console.log(err);
        });
}
/**
 * vrati seznam jednoho roku
 * {"year": "r1892",
    "pages": {
        "0": "r1892_s000.txt",
            ...
        "0_inhalt": "r1892_s000_inhalt.txt"}}
 * @param {string} year rxxxx
 * @param {string} isInhalt inhalt/undefined
 */
function years_get_one(year, isInhalt) {
    const options = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    };
    if (isInhalt == 'inhalt') {
        options.headers.inhalt = isInhalt;
        return fetch('get/' + year, options)
            .then(res => res.json())
            .then(res => {
                return res;
            }).catch(err => {
                console.log(err);
            });
    }
    return fetch('get/' + year, options)
        .then(res => res.json())
        .then(res => {
            return res;
        }).catch(err => {
            console.log(err);
        });
}
/**
 * vraci obsah urcite stranky
 * {"0": {
        "pageSrc": "r1892_s000.txt",
        "jpgSrc": "r1892_s000.jpg",
        "txt": ......
    },"year": "r1892"}
 * @param {string} year rxxxx
 * @param {string} page rxxxx_sxxx.txt
 */
function pages_get_one(year, page) {
    const options = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    };
    return fetch('get/' + year + '/' + page, options)
        .then(res => res.json())
        .then(res => {
            return res;
        }).catch(err => {
            console.log(err);
        });
}
/**
 * vraci seznam src i obsah vsech stranek v rocniku
 * 0: {
 *  0: {
 *      pageSrc: "r1892_s000.txt", 
 *      jpgSrc: "r1892_s000.jpg", 
 *      txt: .....
 *      }
    year: "r1892"
 }
1:{
    1: {
        jpgSrc: "r1892_s001.jpg"
        pageSrc: "r1892_s001.txt"
        txt: .....
        }
    year: "r1892"
}
 * @param {string} year rxxxx
 */
async function pages_get_all_year(year) {
    const el = await years_get_one('r' + year);
    let content = [];
    let tmp;
    for (i in el.pages) {
        tmp = await pages_get_one('r' + year, el.pages[i]);
        content.push(tmp);
    }
    return content;
}
/**
 * vraci obsah inhaltu vsech rocniku
 * @param {string} years rxxxx
 */
async function pages_get_all_inhalts(years) {
    let el;
    let inhalt = [];
    let tmp;
    for (let i = 0; i < years.length; i++) {
        el = await years_get_one('r' + years[i], 'inhalt');
        for (key in el.pages) {
            tmp = await pages_get_one('r' + years[i], el.pages[key]);
            inhalt.push(tmp);
        }
    }
    return inhalt;
}
/**
 * vraci inhalt urciteho roku
 * @param {string} year rxxxx
 */
async function pages_get_inhalt(year) {
    let el;
    let inhalt;
    let tmp;
    el = await years_get_one('r' + year, 'inhalt');
    for (key in el.pages) {
        tmp = await pages_get_one('r' + year, el.pages[key]);
        inhalt = tmp;
    }
    return inhalt;
}