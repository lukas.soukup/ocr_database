var handler = document.getElementById('separator');
var wrapper = handler.closest('.wrapper');
var boxA = wrapper.querySelector('.box');
var isHandlerDragging = false;
let zoom = 3;

//--------------Nastaveni--------------------------------------------------------------------------------------

document.getElementById('nastaveni').onchange = function(e) {
    if (this.checked) {
        document.getElementById("settingsBox").style.display = "block";
    } else {
        document.getElementById("settingsBox").style.display = "none";
    }
};

document.getElementById('autoSave').onchange = function(e) {
    if (document.getElementById('autoSave').checked) {
        document.getElementById('autoSaveText').innerHTML = "&#10003; Zap";
        document.getElementById('autoSaveText').style.color = "green";
        document.getElementById('save').style.display = 'none';
    } else {
        document.getElementById('autoSaveText').innerHTML = "&#10007; Vyp";
        document.getElementById('autoSaveText').style.color = "red";
        document.getElementById('save').style.display = 'block';
    }
};

document.getElementById('autoResults').onchange = function(e) {
    if (this.checked) {
        document.getElementById('autoResultsText').innerHTML = "&#10003; Zap";
        document.getElementById('autoResultsText').style.color = "green";
        document.getElementById("searchAllBtn").style.display = "none";
    } else {
        document.getElementById('autoResultsText').innerHTML = "&#10007; Vyp";
        document.getElementById('autoResultsText').style.color = "red";
        document.getElementById("searchAllBtn").style.display = "block";
    }
};
$(document).mouseup(function(e) {
    var container = $("#settingsBox");

    // If the target of the click isn't the container
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        container.hide();
        document.getElementById("nastaveni").checked = false;
    }
});

//--------------doplnek k interaktivnimu obsahu--------------------------------------------------------------------------------------

document.getElementById("content").onclick = function() {
    let content = document.getElementById("ShowContent");
    if (content.style.width === "40%") {
        content.style.width = "0%";
        content.style.border = "none"
        this.innerHTML = "Zobraz obsah";
        this.style.background = "rgb(59, 19, 133)";
    } else {
        if (document.getElementById("searchAllBtn").style.background == "red") {
            closeSearchAll();
        }
        content.style.width = "40%";
        content.style.border = "gray";
        content.style.borderWidth = "5px";
        content.style.borderStyle = "double";
        this.innerHTML = "Skryj obsah";
        this.style.background = "red";
    }
}

//---------------Vyhledavaci algoritmus-------------------------------------------------------------------------------------

function openSearchAll() {
    let searchResults = document.getElementById("searchAll");

    if (document.getElementById("content").style.background == "red") {
        document.getElementById("content").onclick();
    }
    searchResults.style.width = "155px";
    searchResults.style.border = "gray";
    searchResults.style.borderWidth = "5px";
    searchResults.style.borderStyle = "double";
    document.getElementById("searchAllBtn").style.background = "red";
    if (document.getElementById("autoResults").checked) {
        document.getElementById("search").innerHTML = "&#10060;";
    }
}

function closeSearchAll() {
    let searchResults = document.getElementById("searchAll");
    searchResults.style.width = "0px";
    searchResults.style.border = "none"
    document.getElementById("searchAllBtn").style.background = "rgb(59, 19, 133)";
    if (document.getElementById("autoResults").checked) {
        document.getElementById("search").innerHTML = "&#128270";
    }
}

document.getElementById("searchAllBtn").onclick = function() {
    if (this.style.background != "red") {
        openSearchAll();
        return;
    }
    if (this.style.background == "red") {
        closeSearchAll();
        return;
    }
}

document.getElementById("searchCheckBox").onchange = function() {
    if (this.checked) {
        closeSearchAll();
        return;
    }
    openSearchAll();
    handleInput();
}

function handleInput() {
    removeHighlights();
    let keyWord = document.getElementById("searchText").value;
    let text = document.getElementById("txt").innerHTML;
    if (keyWord == "") {
        closeSearchAll();
        return;
    }
    var re = new RegExp(keyWord, 'gi');
    if (text.search(re) == -1) {
        openSearchAll();
        return;
    }
    document.getElementById("txt").innerHTML = applyHighlights(text, keyWord);
}

function applyHighlights(text, keyWord) {
    var re = new RegExp(keyWord, 'gi');
    return text.replace(re, "<mark>" + keyWord + "</mark>");
}

function removeHighlights() {
    var e = document.getElementById("txt");
    var re = new RegExp(("<mark>"), 'gi');
    var text = e.innerHTML.replace(re, "");
    var re = new RegExp(("</mark>"), 'gi');
    e.innerHTML = text.replace(re, "");
}

document.getElementById("searchText").addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
        event.preventDefault();
        document.getElementById("searchCheckBox").checked = false;
        document.getElementById("searchCheckBox").onchange();
    }
});

//--------------Posuvny prvek mezi obrazkem a textem--------------------------------------------------------------------------------------

document.addEventListener('mousedown', function(e) {
    // If mousedown event is fired from .handler, toggle flag to true
    if (e.target === handler) {
        isHandlerDragging = true;
    }
});

document.addEventListener('mousemove', function(e) {
    // Don't do anything if dragging flag is false
    if (!isHandlerDragging) {
        return false;
    }

    // Get offset
    var containerOffsetLeft = wrapper.offsetLeft;

    // Get x-coordinate of pointer relative to container
    var pointerRelativeXpos = e.clientX - containerOffsetLeft;

    // Arbitrary minimum width set on box A, otherwise its inner content will collapse to width of 0
    var boxAminWidth = 0;

    // Resize box A
    // * 8px is the left/right spacing between .handler and its inner pseudo-element
    // * Set flex-grow to 0 to prevent it from growing
    boxA.style.width = (Math.max(boxAminWidth, pointerRelativeXpos - 8)) + 'px';
    boxA.style.flexGrow = 0;
});

//--------------Lupa--------------------------------------------------------------------------------------

document.addEventListener('mouseup', function(e) {
    // Turn off dragging flag when user mouse is up
    isHandlerDragging = false;
    if (document.getElementById("img-magnifier-glass") != null) {
        magnify(zoom);
    }
});

let magnifier = document.getElementById("magnify");
magnifier.onchange = function() {
    if (this.checked) {
        zoom = 3;
        magnify(zoom);
    } else {
        var child = document.getElementById("img-magnifier-glass");
        var parent = child.parentElement;
        parent.removeChild(child);
    }
};

document.getElementById("zoom-in").onclick = function() {
    if (magnifier.checked && zoom < 10) {
        zoom = zoom + 1;
        magnify(zoom);
    }
};
document.getElementById("zoom-out").onclick = function() {
    if (magnifier.checked && zoom > 1) {
        zoom = zoom - 1;
        magnify(zoom);
    }
};

function magnify(zoom) {
    var glass, w, h, bw;
    let img = document.getElementById("jpg");
    /*create magnifier glass:*/
    glass = document.getElementById("img-magnifier-glass");
    if (glass == null) {
        glass = document.createElement("DIV");
        glass.setAttribute("id", "img-magnifier-glass");
        /*insert magnifier glass:*/
        img.parentElement.insertBefore(glass, img);
        /*set background properties for the magnifier glass:*/
    }
    glass.style.backgroundImage = "url('" + img.src + "')";
    glass.style.backgroundRepeat = "no-repeat";
    glass.style.backgroundSize = (img.width * zoom) + "px " + (img.height * zoom) + "px";
    bw = 3;
    w = glass.offsetWidth / 2;
    h = glass.offsetHeight / 2;
    /*execute a function when someone moves the magnifier glass over the image:*/
    glass.addEventListener("mousemove", moveMagnifier);
    img.addEventListener("mousemove", moveMagnifier);
    /*and also for touch screens:*/
    glass.addEventListener("touchmove", moveMagnifier);
    img.addEventListener("touchmove", moveMagnifier);

    function moveMagnifier(e) {
        var pos, x, y;
        /*prevent any other actions that may occur when moving over the image*/
        e.preventDefault();
        /*get the cursor's x and y positions:*/
        pos = getCursorPos(e);
        x = pos.x;
        y = pos.y;
        /*prevent the magnifier glass from being positioned outside the image:*/
        if (x > img.width - (w / zoom)) { x = img.width - (w / zoom); }
        if (x < w / zoom) { x = w / zoom; }
        if (y > img.height - (h / zoom)) { y = img.height - (h / zoom); }
        if (y < h / zoom) { y = h / zoom; }
        /*set the position of the magnifier glass:*/
        glass.style.left = (x - w) + "px";
        glass.style.top = (y - h) + "px";
        /*display what the magnifier glass "sees":*/
        glass.style.backgroundPosition = "-" + ((x * zoom) - w + bw) + "px -" + ((y * zoom) - h + bw) + "px";
    }

    function getCursorPos(e) {
        var a, x = 0,
            y = 0;
        e = e || window.event;
        /*get the x and y positions of the image:*/
        a = img.getBoundingClientRect();
        /*calculate the cursor's x and y coordinates, relative to the image:*/
        x = e.pageX - a.left;
        y = e.pageY - a.top;
        /*consider any page scrolling:*/
        x = x - window.pageXOffset;
        y = y - window.pageYOffset;
        return { x: x, y: y };
    }
};