function getDir() {
    const fs = require('fs');
    let year = [];
    let i = 0;
    let jpg = "";
    let page = "";
    let pageNum;
    let arrayContent = "<content>";
    let directoryPath = 'D:\\Programovaci aplikace\\VSC programming\\BSProject\\webApp\\Obsah';
    fs.readdirSync(directoryPath).forEach(file => {
        year[i] = file.replace("r","");
        i++;
    });
    for (i = 0; i < year.length; i++) {
        arrayContent = arrayContent.concat("<folder><year>" + year[i] + "</year>");
        fs.readdirSync(directoryPath + "\\r" + year[i]).forEach(file => {
            if (file.endsWith(".txt")) {
                //array.push(file);
                jpg = file.replace(".txt", ".jpg");
                page = file.replace("r" + year[i] + "_","");
                page = page.replace(".txt","");
                pageNum = getPageNum(page);
                arrayContent = arrayContent.concat("<page><pageNumber>" + pageNum + "</pageNumber><txt>Obsah\\\\r" + year[i] + "\\\\" + file + "</txt><jpg>Obsah\\\\r" + year[i] + "\\\\" + jpg + "</jpg></page>");
            }
        });
        arrayContent = arrayContent.concat("</folder>");
    }
    arrayContent = arrayContent.concat("</content>");
    return { arr: arrayContent };
}
function getDirArr() {
    var content = getDir().arr;
    console.log(content);

}
function getPageNum(i) {
    //i = i.slice(0,4);
    let a = i.slice(1,4);
    if (a<10) {
        i = i.replace("s00", "");
    }else if(a<100) {
        i = i.replace("s0", "");
    }else {
        i = i.replace("s", "");
    }
    return i;
}
getDirArr();