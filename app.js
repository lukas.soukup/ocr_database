const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const Routes = require('./api/routes/dataEditing');
const path = require('path');

app.use(express.static('WebContent'));
app.get('/', function(req, res) {
    res.sendFile('index.html', { root: path.join(__dirname, '/WebContent') });
});
app.get('/style.css', function(req, res) {
    res.sendFile('style.css', { root: path.join(__dirname, '/WebContent') });
});
app.get('/main.js', function(req, res) {
    res.sendFile('main.js', { root: path.join(__dirname, '/WebContent') });
});
app.get('/UI.js', function(req, res) {
    res.sendFile('UI.js', { root: path.join(__dirname, '/WebContent') });
});
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//course error handeling 
//umoznuje odesilat req z SPA, atd...
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*"); //umoznuje pristup pro vsechny clienty
    if (req.method === "OPTIONS") {
        res.header("Access-Control-Allow-Methods", "PUT, GET, POST, PATCH, DELETE");
        return res.status(200).json({});
    }
    next(); //pro preposlani do dalsiho routeru
});

app.use('/get', Routes); //tohle preposle vsechny requesty mireny na taks/ukoly do souboru tasks.js
//middleware ktery odchytava requesty mirene na neexistujci odkaz
app.use((req, res, next) => {
    const error = new Error('Not Found');
    error.status = 404;
    next(error);
});

//middleware ktery odchytava vsechny ostatni errory
app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});
module.exports = app;