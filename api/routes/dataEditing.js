const express = require('express');
const router = express.Router();
const WebController = require('../controllers/webReader');

router.get('/', WebController.years_get_all);

router.get('/:year', WebController.years_get_one);

router.get('/:year/:page', WebController.pages_get_content);

module.exports = router;