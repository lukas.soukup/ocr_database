const request = require('request');
const cheerio = require('cheerio');
const url = 'http://kvap.tul.cz/rocenky/';

exports.years_get_all = (req, response, next) => {
    request(url, (err, res, html) => {
        if (!err && res.statusCode == 200) {
            const $ = cheerio.load(html);
            const idk = $('body table tbody tr td a');
            let years = [];
            let j = 0;
            idk.each((i, el) => {
                const item = $(el).text();
                if (item.charAt(0) == 'r') {
                    years[j] = item;
                    j++;
                }
            });
            const Response = {
                count: years.length,
                years: years
            };
            response.status(200).json(Response);
        } else {
            console.log(err);
        }
    });
}

exports.years_get_one = (req, response, next) => {
    request(url + req.params.year, (err, res, html) => {
        if (!err && res.statusCode == 200) {
            const $ = cheerio.load(html);
            const idk = $('body table tbody tr td a');
            let pagesJson = {};
            let arr;
            let page;
            let j = 0;

            idk.each((i, el) => {
                const item = $(el).text();
                if (req.headers.inhalt == 'inhalt') {
                    if (item.endsWith('inhalt.txt')) {
                        arr = item.replace(req.params.year + '_', '');
                        arr = arr.split('.');
                        page = getPageNum(arr[0]);
                        pagesJson[page] = item;
                        return this;
                    }
                } else {
                    if (item.charAt(0) == 'r') {
                        arr = item.replace(req.params.year + '_', '');
                        arr = arr.split('.');
                        page = getPageNum(arr[0]);
                        pagesJson[page] = item;
                        j++;
                    }
                }
            });
            const Response = {
                year: req.params.year,
                pages: pagesJson
            };
            response.status(200).json(Response);
        } else {
            console.log(err);
        }
    });
}

exports.pages_get_content = (req, response, next) => {
    let tmp = req.params.page.split('.');
    if (tmp[1] != 'txt') {
        return response.status(404).json({
            message: "Not Found!"
        });
    }
    request(url + req.params.year + '/' + req.params.page, (err, res, html) => {
        if (!err && res.statusCode == 200) {
            const $ = cheerio.load(html);
            const idk = $('body').text();
            let jpg = req.params.page.replace('.txt', '.jpg');
            let arr = req.params.page.replace(req.params.year + '_', '');
            arr = arr.split('.');
            let page = getPageNum(arr[0]);
            const Response = {
                year: req.params.year
            };
            Response[page] = {
                pageSrc: req.params.page,
                jpgSrc: jpg,
                txt: idk
            };
            response.status(200).json(Response);
        } else {
            console.log(err);
        }
    });
}

function getPageNum(i) {
    let a = i.slice(1, 4);
    if (a < 10) {
        i = i.replace("s00", "");
    } else if (a < 100) {
        i = i.replace("s0", "");
    } else {
        i = i.replace("s", "");
    }
    return i;
}

/*console.log(years);

const fs = require('fs');


app.post('/api', (request, response) => {
    let path = request.body.path;
    let content = request.body.content;
    fs.writeFile(path, content, (err) => {
        if (err) throw err;
        console.log('File saved as ' + path);
    });
    response.json({
        status: 'File saved',
        body: {
            Path: request.body.path,
            content: request.body.text
        }
    });
});

app.post('/api2', (request, response) => {
    console.log("incomming request");
    let files = request.body.array;
    let content = []
    for (let i = 0; i < files.length; i++) {
        content[i] = fs.readFileSync(files[i], "utf8");
    }
    console.log("files like " + files[0] + " etc...  Successfuly loaded");
    response.json({
        status: 'successful',
        body: { array: content }
    });
});*/